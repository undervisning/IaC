
# Install Choco:
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

# Set.Execution Policy
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine

# PowerShell
choco install powershell-core

# Azure CLI
choco install azure-cli

# Terraform
choco install terraform

# VS Code
choco install vscode