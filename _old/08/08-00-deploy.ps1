<#$tenantid = "02feabb9-444e-4f66-9c13-6a8f04b75c2f"
$subscriptionid ="efc1e7b1-5729-4eea-b33e-12cc6b1c0183"
$credentials = Get-Credential
Connect-AzAccount -ServicePrincipal -Credential $credentials -TenantId $tenantid -Environment AzureCloud -SubscriptionId $subscriptionid
#>

Connect-AzAccount
Set-AzContext -Tenantid fa615eb7-bcfa-417d-abf3-849ff0aba680
Set-AzContext -Subscription efc1e7b1-5729-4eea-b33e-12cc6b1c0183

$rg = "iac-demo-rg-tim"
New-AzResourceGroup -Name $rg -Location 'West Europe' -Force

New-AzResourceGroupDeployment `
    -Name "new-storage-iac-demo" `
    -ResourceGroupName $rg `
    -TemplateFile '' `
    -TemplateParameterFile ''
