$tenantid = '02feabb9-444e-4f66-9c13-6a8f04b75c2f'
$subscriptionid = 'efc1e7b1-5729-4eea-b33e-12cc6b1c0183'
$credentials = Get-Credential
Connect-AzAccount -ServicePrincipal -Credential $credentials -TenantId $tenantid -Environment AzureCloud -SubscriptionId $subscriptionid

$rg = "<din resource groupe>"
$location = "West Europe"
New-AzResourceGroup -Name $rg -Location $location -Force

$storageAccount = New-AzStorageAccount -ResourceGroupName $rg `
  -Name "<dittsanavn, bare små bokstaver og tall>" `
  -SkuName Standard_LRS `
  -Location $location 

$ctx = $storageAccount.Context

$containerName = "<ditt containernavn>"
New-AzStorageContainer -Name $containerName -Context $ctx -Permission blob

Set-AzStorageBlobContent -File "C:\<dinstitilfila>\arm-public.json" -Container $containerName `
  -Blob "arm-public.json" `
  -Context $ctx `
  -StandardBlobTier Hot
#MERK: Blog er det samme som filnavnet

#Remove-AzResourceGroup -name $rg -Force:$true
