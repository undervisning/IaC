terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.53.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg-vnet" {
  name     = "rg-demo-vnet"
  location = "West Europe"
}

resource "azurerm_virtual_network" "demo-vnet" {
  name                = "vnet-demo-001-we"
  location            = azurerm_resource_group.rg-vnet.location
  resource_group_name = azurerm_resource_group.rg-vnet.name
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "demo-sn" {
  name                 = "sn-01-vnet001-we"
  resource_group_name  = azurerm_resource_group.rg-vnet.name
  virtual_network_name = azurerm_virtual_network.demo-vnet.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_network_security_group" "nsg-demo" {
  name                = "nsg-sn01-demo"
  location            = azurerm_resource_group.rg-vnet.location
  resource_group_name = azurerm_resource_group.rg-vnet.name

  security_rule {
    name                       = "Allow-Public-IP"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "178.164.104.51"
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "example" {
  subnet_id                 = azurerm_subnet.demo-sn.id
  network_security_group_id = azurerm_network_security_group.nsg-demo.id
}

resource "azurerm_public_ip" "pip-demo" {
  name                = "pip-demo-linux"
  resource_group_name = azurerm_resource_group.rg-vnet.name
  location            = azurerm_resource_group.rg-vnet.location
  allocation_method   = "Static"

}

resource "azurerm_network_interface" "nic-demo" {
  name                = "nic-demo-linux"
  location            = azurerm_resource_group.rg-vnet.location
  resource_group_name = azurerm_resource_group.rg-vnet.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.demo-sn.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.pip-demo.id
  }
}

resource "azurerm_linux_virtual_machine" "example" {
  name                = "example-machine"
  resource_group_name = azurerm_resource_group.rg-vnet.name
  location            = azurerm_resource_group.rg-vnet.location
  size                = "Standard_F2"
  admin_username      = "adminuser"
  admin_password      = "fdfW!dfd_sds123"
  disable_password_authentication = false
  network_interface_ids = [
    azurerm_network_interface.nic-demo.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
}
