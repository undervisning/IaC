echo "" > 02-storage.json
echo "" > 02-storage.parameters.json

# Erstatt med informasjonen med din resource group og dine filer under --template-file og --parameters:

group='<navn på resource group>'

az group create -g $group -l westeurope

az group deployment create \
    -g $group \
    --template-file 05-02-demo-storage-shell.json \
    --parameters @05-02-demo-storage-shell.parameters.json

az group deployment create \
    -g $group \
    --template-file 05-02-demo-storage-shell.json \
    --parameters @05-02-demo-storage-shell.parameters.json \
    --parameters storageAccountSKU=Standard_GRS
    