echo "" > 03-storage.json
echo "" > 03-storage.parameters-dev.json
echo "" > 03-storage.parameters-prod.json

# Erstatt med informasjonen med din resource group og dine filer under --template-file og --parameters:

group='<navn på resource group>'

az group create -g $group -l westeurope

az group deployment create \
    -g $group \
    --template-file 05-03-demo-storage-code.json \
    --parameters @05-03-demo-storage-code.parameters-dev.json

group='<navn på resource group>'

az group create -g $group -l westeurope

az group deployment create \
    -g $group \
    --template-file 05-03-demo-storage-code.json \
    --parameters @05-03-demo-storage-code.parameters-prod.json