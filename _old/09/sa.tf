provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "example" {
  name     = "example-resources"
  location = "West Europe"
}

resource "azurerm_storage_account" "example" {
  name                     = "examplestoraccount"
  resource_group_name      = azurerm_resource_group.example.name
  location                 = azurerm_resource_group.example.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "test"
  }
}


provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "example" {
  name     = "example-resources"
  location = "West Europe"
}

variable "instance_type" {
  description = "The type of the virtual machine."
  default     = "Standard_B1s"
}

module "network" {
  source = "./modules/network"
}

provider "azurerm" {
  features {}
}

variable "resource_group_name" {
  type        = string
  description = "The name of the resource group"
  default     = "my-resource-group"
}

resource "azurerm_resource_group" "example" {
  name     = var.resource_group_name
  location = "West Europe"
}


resource "azurerm_virtual_machine" "my_vm" {
  name = "my-vm"
  # andre konfigurasjonsalternativer for den virtuelle maskinen
}

resource "azurerm_virtual_machine" "my_vm" {
  name = "my-vm"
  # andre konfigurasjonsalternativer for den virtuelle maskinen
}

resource "azurerm_network_security_group" "my_nsg" {
  name                = "my-nsg"
  location            = "West Europe"
  resource_group_name = "my-resource-group"

  security_rule {
    name                       = "allow_ssh"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
    description                = "Allow SSH"
    # objektreferansen til den virtuelle maskinen
    destination_application_security_group_ids = [azurerm_virtual_machine.my_vm.id]
  }
}

