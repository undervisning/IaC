provider "azurerm" {
    features {}
}

resource "azurerm_resource_group" "sa" {
    name                        = "rg-storage-account"
    location                    = "westeurope"
}

resource "azurerm_storage_account" "sa" {
    name                        = "tim123qweasd"
    resource_group_name         = azurerm_resource_group.sa.name
    location                    = azurerm_resource_group.sa.location
    account_tier                = "Standard"
    account_replication_type    = "LRS"


    tags = {
        environment             = "test"
    }   
} 